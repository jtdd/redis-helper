# Redis Helper

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

#### Software Architecture
Software architecture description

#### Installation

```Bash
go get -u gitee.com/jtdd/redis-helper
```

#### Instructions

```go
package main

import "gitee.com/jtdd/redis-helper/src/v1"
```

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request

