package v1

import (
	"testing"

	"github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/stretchr/testify/assert"
)

var (
	ctx         = gctx.New()
	redisHelper = NewRedisHelper(ctx, "", false)
)

func init() {
	// 准备测试环境
	config := gredis.Config{
		Address: "127.0.0.1:6379",
		Db:      15,
		Pass:    "1234",
	}
	gredis.RegisterAdapterFunc(func(config *gredis.Config) gredis.Adapter {
		return redis.New(config)
	})
	gredis.SetConfig(&config)
}

func TestRedisHelper_Watch(t *testing.T) {
	// Define the watch callback function
	callback := func(key string, helper *RedisHelper) error {
		// Perform some operations within the watch callback
		return nil
	}

	err := redisHelper.Watch("testKey", callback)
	if err != nil {
		t.Errorf("Watch returned an error: %v", err)
	}
}

func TestRedisHelper_Multi(t *testing.T) {
	// Define the multi callback function
	callback := func(helper *RedisHelper) error {
		// Perform some operations within the multi callback
		return nil
	}

	v, err := redisHelper.Multi(callback)
	if err != nil {
		t.Errorf("Multi returned an error: %v", err)
	}

	if v.Val() != 1 {
		t.Errorf("Expected v to be 1, got %v", v.Val())
	}
}

func TestRedisHelper_ScriptRunner(t *testing.T) {
	scriptRunner := redisHelper.SetPrefix("test:").SetEnabledPrefix(true).ScriptRunner()
	if scriptRunner == nil {
		t.Errorf("ScriptRunner returned nil")
	}
	_, err := scriptRunner.AddScript("testScript", "return 'Hello, World!'")
	assert.NoError(t, err)
	v, err := scriptRunner.RunScript("testScript", []string{}, []interface{}{})
	assert.NoError(t, err)
	assert.Equal(t, "Hello, World!", v.String())
}
