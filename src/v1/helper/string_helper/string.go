package string_helper

import (
	"time"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/gogf/gf/v2/util/gutil"
)

// StringHelper Redis String类型助手类
type StringHelper struct {
	*base.Base
}

// NewStringHelper 实例化
func NewStringHelper(b *base.Base) *StringHelper {
	return &StringHelper{
		Base: b,
	}
}

func (s StringHelper) Get(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "GET", s.GetKey(key))

	return
}

func (s StringHelper) GetNoError(key string) (v *gvar.Var) {
	v, _ = s.Get(s.GetKey(key))

	return
}

func (s StringHelper) Getbit(key string, offset interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "GETBIT", s.GetKey(key), offset)

	return
}

func (s StringHelper) Getrange(key string, start int64, end int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "GETRANGE", s.GetKey(key), start, end)

	return
}

func (s StringHelper) MGet(keys []string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "MGET", gconv.SliceAny(s.GetKeys(keys))...)

	return
}

func (s StringHelper) Set(key string, value string, duration ...time.Duration) (v *gvar.Var, err error) {
	if duration != nil && len(duration) > 0 {
		v, err = g.Redis().Do(s.Ctx, "SET", s.GetKey(key), value, "EX", gconv.Uint64(duration[0].Seconds()))
	} else {
		v, err = g.Redis().Do(s.Ctx, "SET", s.GetKey(key), value)
	}

	return
}

func (s StringHelper) SetOnlyError(key string, value string, duration ...time.Duration) (err error) {
	_, err = s.Set(s.GetKey(key), value, duration...)

	return
}

func (s StringHelper) SetNx(key string, value string, duration ...time.Duration) (v *gvar.Var, err error) {
	if duration != nil && len(duration) > 0 {
		v, err = g.Redis().Do(s.Ctx, "SETNX", s.GetKey(key), value, "EX", gconv.Uint64(duration[0].Seconds()))
	} else {
		v, err = g.Redis().Do(s.Ctx, "SETNX", s.GetKey(key), value)
	}

	return
}

func (s StringHelper) MSet(kv g.Map) (v *gvar.Var, err error) {
	if s.IsEnabledPrefix {
		for k, v := range kv {
			kv[s.GetKey(k)] = v
		}
	}
	v, err = g.Redis().Do(s.Ctx, "MSET", gutil.MapToSlice(kv)...)

	return
}

func (s StringHelper) Incr(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "INCR", s.GetKey(key))

	return
}

func (s StringHelper) Incrby(key string, value int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "INCRBY", s.GetKey(key), value)

	return
}

func (s StringHelper) Decr(key string, value int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "DECR", s.GetKey(key), value)

	return
}

func (s StringHelper) Decrby(key string, value int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "DECRBY", s.GetKey(key), value)

	return
}

func (s StringHelper) Getset(key string, value string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "GETSET", s.GetKey(key), value)

	return
}
