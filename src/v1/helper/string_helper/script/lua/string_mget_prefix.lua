local prefix = ARGV[1]
local keys = {}
for i = 2, #ARGV do
    table.insert(keys, prefix..tostring(ARGV[i]))
end
return redis.call('MGET', unpack(keys))
