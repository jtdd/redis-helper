package script

import _ "embed"

const (
	ScriptStringMGetPrefixName = "string_mget_prefix"
)

var (
	// ScriptStringMGetPrefix
	// Hash Map IUD
	//go:embed lua/string_mget_prefix.lua
	ScriptStringMGetPrefix string
)
