package string_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"gitee.com/jtdd/redis-helper/src/v1/helper/script_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/string_helper/script"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/util/gconv"
)

type StringScriptHelper struct {
	*base.Base
	scriptRunnerHelper *script_helper.ScriptRunner
}

func NewStringScriptHelper(b *base.Base) *StringScriptHelper {
	return &StringScriptHelper{
		Base:               b,
		scriptRunnerHelper: script_helper.NewScriptRunner(b),
	}
}

// MGetWithPrefixScript 批量获取keys
func (s StringScriptHelper) MGetWithPrefixScript(prefixKey string, keys []string) (v *gvar.Var, err error) {
	// 调用script_helper 当心交叉调用 之后再优化
	args := []interface{}{s.GetKey(prefixKey)} // 第一个参数占位
	args = append(args, gconv.SliceAny(keys)...)

	v, err = s.scriptRunnerHelper.RunAutoLoadScript(
		script.ScriptStringMGetPrefixName,
		script.ScriptStringMGetPrefix,
		[]string{},
		args)
	if err != nil {
		return
	}

	return
}
