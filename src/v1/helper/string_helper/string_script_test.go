package string_helper

import (
	"context"
	"testing"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/stretchr/testify/assert"
)

func init() {
	// 准备测试环境
	config := gredis.Config{
		Address: "127.0.0.1:6379",
		Db:      15,
		Pass:    "1234",
	}
	gredis.RegisterAdapterFunc(func(config *gredis.Config) gredis.Adapter {
		return redis.New(config)
	})
	gredis.SetConfig(&config)
}

func TestMGetWithPrefixScript(t *testing.T) {
	// Create a test context
	ctx := context.Background()
	b := base.NewBase(ctx, "t:", true)

	// Create a new HashMapIUDHelper instance with the mock dependencies
	helper := NewStringScriptHelper(b)

	// Prepare test data
	keys := []string{
		"key1",
		"key2",
	}

	// Call the MGetWithPrefixScript method
	v, err := helper.MGetWithPrefixScript("s:", keys)

	// assert.NotNil(t, v)

	// print v
	t.Log(v)

	// Assert that there was no error
	assert.NoError(t, err)
}
