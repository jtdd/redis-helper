package key_helper

import (
	"time"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

// KeyHelper Redis String类型助手类
type KeyHelper struct {
	*base.Base
}

// NewKeyHelper 实例化
func NewKeyHelper(b *base.Base) *KeyHelper {
	return &KeyHelper{
		Base: b,
	}
}

func (s KeyHelper) Exists(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "EXISTS", s.GetKey(key))

	return
}

func (s KeyHelper) ExistsNoError(key string) (v *gvar.Var) {
	v, _ = s.Exists(key)

	return
}

func (s KeyHelper) ExistsBool(key string) (b bool, err error) {
	var v *gvar.Var
	v, err = s.Exists(key)
	if err != nil {
		return
	}

	b = v.Bool()

	return
}

func (s KeyHelper) Del(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "DEL", s.GetKey(key))

	return
}

func (s KeyHelper) DelOnlyError(key string) (err error) {
	_, err = s.Exists(s.GetKey(key))

	return
}

func (s KeyHelper) DelM(keys []string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "DEL", g.SliceAny{s.GetKeys(keys)}...)

	return
}

func (s KeyHelper) Expire(key string, seconds time.Duration) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "EXPIRE", s.GetKey(key), gconv.Uint64(seconds.Seconds()))

	return
}

func (s KeyHelper) Ttl(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "TTL", s.GetKey(key))

	return
}

func (s KeyHelper) TtlInt(key string) (ttl int64, err error) {
	var v *gvar.Var
	v, err = s.Ttl(key)
	if err != nil {
		return
	}

	ttl = v.Int64()

	return
}

func (s KeyHelper) ExpireAt(key string, timestamp time.Time) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "EXPIREAT", s.GetKey(key), gconv.Uint64(timestamp.Unix()))

	return
}

func (s KeyHelper) Persist(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "PERSIST", s.GetKey(key))

	return
}

// Scan 批量获取key 【注意】key里需要有通配符 比如：a:*
func (s KeyHelper) Scan(key string, callback func(keys []string) error, count ...int) (err error) {
	_count := 100
	if count != nil && len(count) > 0 {
		_count = count[0]
	}

	var v *gvar.Var

	cursor := 0
	for {
		v, err = g.Redis().Do(s.Ctx, "SCAN", cursor, "MATCH", s.GetKey(key), "COUNT", gconv.String(_count))
		if err != nil {
			return
		}
		data := gconv.SliceAny(v)
		var dataSlice []string
		err = gconv.Scan(data[1], &dataSlice)
		if err != nil {
			return
		}

		err = callback(dataSlice)
		if err != nil {
			return
		}

		cursor = gconv.Int(data[0])
		if cursor == 0 {
			break
		}
	}

	return
}
