package script_helper

import (
	"errors"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gmap"
	"github.com/gogf/gf/v2/container/gvar"
)

type ScriptRunner struct {
	*base.Base
	ScriptHelper *ScriptHelper

	// 脚本列表 name -> ScriptRunnerItem
	Scripts *gmap.Map
}

type ScriptRunnerItem struct {
	Script string
	Sha1   string
}

const (
	// redis 返回错误值
	RedisErrNoScript = "NOSCRIPT No matching script. Please use EVAL."
)

var (
	// 脚本不存在
	ErrScriptNotExist = errors.New("Script Not Exist")
)

func NewScriptRunner(b *base.Base) *ScriptRunner {
	return &ScriptRunner{
		Base:         b,
		ScriptHelper: NewScriptHelper(b),
		Scripts:      gmap.New(true),
	}
}

func NewScriptRunnerItem(script, sha1 string) *ScriptRunnerItem {
	return &ScriptRunnerItem{
		Script: script,
		Sha1:   sha1,
	}
}

func (s *ScriptRunner) SearchScript(name string) (item *ScriptRunnerItem, ok bool) {
	var v interface{}
	v, ok = s.Scripts.Search(name)
	if !ok {
		return
	}

	item = v.(*ScriptRunnerItem)
	return
}

func (s *ScriptRunner) AddScript(name string, script string) (item *ScriptRunnerItem, err error) {
	var sha1 string
	sha1, err = s.ScriptHelper.ScriptLoad(script)
	if err != nil {
		return
	}
	item = NewScriptRunnerItem(script, sha1)
	s.Scripts.Set(name, item)
	return
}

func (s *ScriptRunner) SetScript(name string, script string) (err error) {
	item, ok := s.Scripts.Search(name)
	if !ok {
		err = ErrScriptNotExist
		return
	}

	var sha1 string
	sha1, err = s.ScriptHelper.ScriptLoad(script)
	if err != nil {
		return
	}

	runner := item.(*ScriptRunnerItem)
	runner.Script = script
	runner.Sha1 = sha1

	s.Scripts.Set(name, runner)
	return
}

func (s *ScriptRunner) LoadScript(name string) (err error) {
	item, ok := s.Scripts.Search(name)
	if !ok {
		err = ErrScriptNotExist
		return
	}

	runner := item.(*ScriptRunnerItem)

	var sha1 string
	sha1, err = s.ScriptHelper.ScriptLoad(runner.Script)
	if err != nil {
		return
	}

	runner.Sha1 = sha1

	s.Scripts.Set(name, runner)
	return
}

func (s *ScriptRunner) ReLoadAllScripts() (err error) {
	s.Scripts.Iterator(func(k interface{}, v interface{}) bool {
		name := k.(string)
		runner := v.(*ScriptRunnerItem)

		var sha1 string
		sha1, err = s.ScriptHelper.ScriptLoad(runner.Script)
		if err != nil {
			// 忽略错误 继续下一个
			return true
		}

		runner.Sha1 = sha1

		s.Scripts.Set(name, runner)
		return true
	})
	return
}

func (s *ScriptRunner) ReLoadWithout() (err error) {
	s.Scripts.Iterator(func(k interface{}, v interface{}) bool {
		name := k.(string)
		runner := v.(*ScriptRunnerItem)

		if runner.Sha1 != "" {
			return true
		}

		var sha1 string
		sha1, err = s.ScriptHelper.ScriptLoad(runner.Script)
		if err != nil {
			// 忽略错误 继续下一个
			return true
		}

		runner.Sha1 = sha1

		s.Scripts.Set(name, runner)
		return true
	})
	return
}

func (s *ScriptRunner) RunScript(name string, keys []string, args []interface{}) (v *gvar.Var, err error) {
	runScript := func() (v *gvar.Var, err error) {
		item, ok := s.Scripts.Search(name)
		if !ok {
			err = ErrScriptNotExist
			return
		}

		v, err = s.ScriptHelper.EvalSha(item.(*ScriptRunnerItem).Sha1, keys, args)
		return
	}

	v, err = runScript()
	if err != nil && err.Error() == RedisErrNoScript {
		err = s.LoadScript(name)
		if err != nil {
			return
		}

		v, err = runScript()
		if err != nil {
			return
		}
	}

	return
}

func (s *ScriptRunner) RunAutoLoadScript(name, script string, keys []string, args []interface{}) (v *gvar.Var, err error) {
	_, ok := s.SearchScript(name)
	if !ok {
		// 定义 Lua 脚本
		_, err = s.AddScript(name, script)
		if err != nil {
			return
		}
	}

	v, err = s.RunScript(name, keys, args)
	if err != nil {
		return
	}

	return
}
