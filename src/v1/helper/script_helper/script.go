package script_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

type ScriptHelper struct {
	*base.Base
}

func NewScriptHelper(b *base.Base) *ScriptHelper {
	return &ScriptHelper{
		Base: b,
	}
}

func (s ScriptHelper) Eval(script string, keys []string, args []interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().Eval(s.Ctx, script, gconv.Int64(len(keys)), s.GetKeys(keys), args)

	return
}

func (s ScriptHelper) EvalSha(sha1 string, keys []string, args []interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().EvalSha(s.Ctx, sha1, gconv.Int64(len(keys)), s.GetKeys(keys), args)

	return
}

func (s ScriptHelper) ScriptLoad(script string) (v string, err error) {
	v, err = g.Redis().ScriptLoad(s.Ctx, script)

	return
}

func (s ScriptHelper) ScriptExists(sha1 string, sha1s ...string) (v map[string]bool, err error) {
	v, err = g.Redis().ScriptExists(s.Ctx, sha1, sha1s...)

	return
}

func (s ScriptHelper) ScriptFlush(option ...gredis.ScriptFlushOption) (err error) {
	err = g.Redis().ScriptFlush(s.Ctx, option...)

	return
}

func (s ScriptHelper) ScriptKill() (err error) {
	err = g.Redis().ScriptKill(s.Ctx)

	return
}
