package base

import (
	"reflect"
	"time"
	
	"github.com/gogf/gf/v2/errors/gerror"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/gogf/gf/v2/util/gconv"
)

// GenericsVarStructHelper 泛型值转换
type GenericsVarStructHelper struct {
}

const (
	ValueTypeInt     = "int"
	ValueTypeInt8    = "int8"
	ValueTypeInt16   = "int16"
	ValueTypeInt32   = "int32"
	ValueTypeInt64   = "int64"
	ValueTypeUint    = "uint"
	ValueTypeUint8   = "uint8"
	ValueTypeUint16  = "uint16"
	ValueTypeUint32  = "uint32"
	ValueTypeUint64  = "uint64"
	ValueTypeFloat32 = "float32"
	ValueTypeFloat64 = "float64"
	ValueTypeBool    = "bool"
	ValueTypeString  = "string"
	ValueTypeTime    = "time.Time"
	ValueTypeTimeP   = "*time.Time"
	ValueTypeGTime   = "gtime.Time"
	ValueTypeGTimeP  = "*gtime.Time"
)

func NewGenericsVarStructHelper() *GenericsVarStructHelper {
	return &GenericsVarStructHelper{}
}

// SetField 是一个泛型函数，用于设置任何结构体中指定字段的值。
func SetField[T any](t *T, fieldName string, value interface{}) (err error) {
	// 使用反射来访问和修改结构体字段
	val := reflect.ValueOf(t).Elem()
	
	// 如果输入是接口，尝试解包
	if val.Kind() == reflect.Interface && !val.IsNil() {
		val = val.Elem()
	}
	// 如果输入是值类型，需要取其地址
	// if val.Kind() != reflect.Ptr {
	// 	val = reflect.ValueOf(&t).Elem()
	// }
	
	// if val.Kind() != reflect.Struct {
	// 	err = gerror.New("input is not a struct")
	// 	return
	// }
	
	field := val.FieldByName(fieldName)
	if !field.IsValid() {
		err = gerror.Newf("field %s does not exist in the struct", fieldName)
		return
	}
	
	if !field.CanSet() {
		err = gerror.Newf("field %s is not settable", fieldName)
		return
	}
	
	// 检查值类型是否匹配
	if field.Type() != reflect.TypeOf(value) {
		err = gerror.New("value type does not match the field type")
		return
	}
	
	field.Set(reflect.ValueOf(value))
	return
}

// Var2ValueByType 反射将 gvar.Var 转换为结构体字段的值
func (s *GenericsVarStructHelper) Var2ValueByType(v *g.Var, val reflect.Value, fieldName string, vType string) (err error) {
	field := val.FieldByName(fieldName)
	
	if !field.IsValid() {
		err = gerror.Newf("field %s does not exist in the struct", fieldName)
		return
	}
	
	if !field.CanSet() {
		err = gerror.Newf("field %s is not settable", fieldName)
		return
	}
	
	switch vType {
	case ValueTypeInt:
		field.Set(reflect.ValueOf(v.Int()))
	case ValueTypeInt8:
		field.Set(reflect.ValueOf(v.Int8()))
	case ValueTypeInt16:
		field.Set(reflect.ValueOf(v.Int16()))
	case ValueTypeInt32:
		field.Set(reflect.ValueOf(v.Int32()))
	case ValueTypeInt64:
		field.Set(reflect.ValueOf(v.Int64()))
	case ValueTypeUint:
		field.Set(reflect.ValueOf(v.Uint()))
	case ValueTypeUint8:
		field.Set(reflect.ValueOf(v.Uint8()))
	case ValueTypeUint16:
		field.Set(reflect.ValueOf(v.Uint16()))
	case ValueTypeUint32:
		field.Set(reflect.ValueOf(v.Uint32()))
	case ValueTypeUint64:
		field.Set(reflect.ValueOf(v.Uint64()))
	case ValueTypeFloat32:
		field.Set(reflect.ValueOf(v.Float32()))
	case ValueTypeFloat64:
		field.Set(reflect.ValueOf(v.Float64()))
	case ValueTypeBool:
		field.Set(reflect.ValueOf(v.Bool()))
	case ValueTypeString:
		field.Set(reflect.ValueOf(v.String()))
	case ValueTypeTime:
		field.Set(reflect.ValueOf(time.UnixMilli(v.Int64())))
	case ValueTypeTimeP:
		t := time.UnixMilli(v.Int64())
		field.Set(reflect.ValueOf(&t))
	case ValueTypeGTime:
		gt := gtime.NewFromTimeStamp(v.Int64())
		field.Set(reflect.ValueOf(*gt))
	case ValueTypeGTimeP:
		gt := gtime.NewFromTimeStamp(v.Int64())
		field.Set(reflect.ValueOf(gt))
	default:
		err = gerror.New("value type does not match the field type")
		return
	}
	
	return
}

// Reflect2ValueByType 反射获取值 用于写入redis 需将特殊类型转换字符串或整形时间戳等 总之必须是Redis支持的类型 其实最终都是string
func (s *GenericsVarStructHelper) Reflect2ValueByType(val reflect.Value, fieldName string, vType string) (value interface{}, err error) {
	field := val.FieldByName(fieldName)
	
	if !field.IsValid() {
		err = gerror.Newf("field %s does not exist in the struct", fieldName)
		return
	}
	
	if !field.CanSet() {
		err = gerror.Newf("field %s is not settable", fieldName)
		return
	}
	
	switch vType {
	case ValueTypeInt, ValueTypeInt8, ValueTypeInt16, ValueTypeInt32, ValueTypeInt64:
		value = field.Int()
	case ValueTypeUint, ValueTypeUint8, ValueTypeUint16, ValueTypeUint32, ValueTypeUint64:
		value = field.Uint()
	case ValueTypeFloat32, ValueTypeFloat64:
		value = field.Float()
	case ValueTypeBool:
		value = gconv.Int(field.Bool())
	case ValueTypeString:
		value = field.String()
	case ValueTypeTime, ValueTypeTimeP, ValueTypeGTime, ValueTypeGTimeP:
		value = gconv.GTime(field.Interface()).TimestampMilli()
	default:
		err = gerror.New("value type does not match the field type")
		return
	}
	
	return
}
