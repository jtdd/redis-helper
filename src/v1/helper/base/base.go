package base

import "context"

type Base struct {
	Ctx    context.Context
	Prefix string

	IsEnabledPrefix bool
}

func NewBase(ctx context.Context, prefix string, isEnabledPrefix bool) *Base {
	return &Base{
		Ctx:    ctx,
		Prefix: prefix,

		IsEnabledPrefix: isEnabledPrefix,
	}
}

func (s *Base) GetKey(key string) string {
	if s.IsEnabledPrefix {
		return s.Prefix + key
	}

	return key
}

func (s *Base) GetKeys(keys []string) []string {
	_keys := make([]string, len(keys))
	for i, key := range keys {
		_keys[i] = s.GetKey(key)
	}
	return _keys
}
