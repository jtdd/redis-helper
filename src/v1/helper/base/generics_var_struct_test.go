package base

import (
	"reflect"
	"testing"
	"time"
	
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/test/gtest"
	"github.com/stretchr/testify/assert"
)

type TestStruct struct {
	Field1 int
	Field2 string
	Field3 time.Time
}

func TestSetField(t *testing.T) {
	ts := &TestStruct{Field1: 1, Field2: "initial"}
	
	// 测试成功设置整型字段
	err := SetField(ts, "Field1", 10)
	assert.NoError(t, err)
	assert.Equal(t, 10, ts.Field1)
	
	// 测试尝试设置不存在的字段
	err = SetField(ts, "NonExistentField", "someValue")
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "field NonExistentField does not exist")
	
	// 测试类型不匹配
	err = SetField(ts, "Field2", 123)
	assert.Error(t, err)
	assert.Contains(t, err.Error(), "value type does not match the field type")
}

func TestVar2ValueByType(t *testing.T) {
	gtest.C(t, func(t *gtest.T) {
		valueInt := 100
		valueString := "hello"
		valueTime := time.Now()
		
		helper := NewGenericsVarStructHelper()
		val := reflect.ValueOf(&TestStruct{}).Elem()
		// 如果输入是接口，尝试解包
		// if val.Kind() == reflect.Interface && !val.IsNil() {
		// 	val = val.Elem()
		// }
		// 如果输入是值类型，需要取其地址
		// if val.Kind() != reflect.Ptr {
		// 	val = reflect.ValueOf(&t).Elem()
		// }
		
		// 测试正确类型转换
		variable := g.NewVar(valueInt)
		err := helper.Var2ValueByType(variable, val, "Field1", ValueTypeInt)
		t.AssertNil(err)
		t.Assert(val.FieldByName("Field1").Int(), int(valueInt))
		
		variable = g.NewVar(valueString)
		err = helper.Var2ValueByType(variable, val, "Field2", ValueTypeString)
		t.AssertNil(err)
		t.Assert(val.FieldByName("Field2").String(), valueString)
		
		variable = g.NewVar(valueTime)
		err = helper.Var2ValueByType(variable, val, "Field3", ValueTypeTime)
		t.AssertNil(err)
		t.Assert(val.FieldByName("Field3").Interface(), valueTime)
		
		// 类型不匹配的测试应根据实际情况编写，这里假设 Var 类型不直接支持所有转换测试
	})
}
