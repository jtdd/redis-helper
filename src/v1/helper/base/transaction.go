package base

import (
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
)

// TransactionHelper Redis Transaction类型助手类
type TransactionHelper struct {
	*Base
}

// NewTransactionHelper 实例化
func NewTransactionHelper(base *Base) *TransactionHelper {
	return &TransactionHelper{
		Base: base,
	}
}

func (s TransactionHelper) Watch(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "WATCH", key)

	return
}

func (s TransactionHelper) UnWatch() (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "UNWATCH")

	return
}

func (s TransactionHelper) Multi() (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "MULTI")

	return
}

func (s TransactionHelper) Exec() (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "EXEC")

	return
}

func (s TransactionHelper) Discard() (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "DISCARD")

	return
}
