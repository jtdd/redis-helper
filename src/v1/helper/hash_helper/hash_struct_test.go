package hash_helper

import (
	"testing"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gstructs"
	"github.com/gogf/gf/v2/os/gtime"
	"github.com/stretchr/testify/assert"
)

// 假设你有这样一个结构体用于测试
type TestUser struct {
	Name string      `redis_hash_field:"name"`
	Age  int         `redis_hash_field:"age"`
	Time *gtime.Time `redis_hash_field:"time"`
	Bool bool        `redis_hash_field:"bool"`
}

func init() {
	// 准备测试环境
	config := gredis.Config{
		Address: "127.0.0.1:6379",
		Db:      15,
		Pass:    "1234",
	}
	gredis.RegisterAdapterFunc(func(config *gredis.Config) gredis.Adapter {
		return redis.New(config)
	})
	gredis.SetConfig(&config)
}

func TestNewGenericsVarStructHelper(t *testing.T) {
	tags, err := gstructs.TagFields(&TestUser{}, []string{DefaultTagHashField})

	// 验证结果
	assert.NoError(t, err)
	g.Dump(tags)
}

func TestHashStructHelper_Get(t *testing.T) {
	// group := "cache"
	ctx := gctx.New()
	b := base.NewBase(ctx, "testPrefix:", true)

	helper := NewHashStructHelper[TestUser](b)

	// 执行测试
	user, err := helper.Get("testKey")

	// 验证结果
	assert.NoError(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, "Alice", user.Name)
	assert.Equal(t, 30, user.Age)
	assert.Equal(t, "2023-07-08 08:00:00", user.Time.String())
	assert.Equal(t, true, user.Bool)
}

func TestHashStructHelper_Set(t *testing.T) {
	ctx := gctx.New()
	b := base.NewBase(ctx, "testPrefix:", true)

	helper := NewHashStructHelper[TestUser](b)

	// 执行测试
	err := helper.Set("testKey", &TestUser{
		Name: "Alice1",
		Age:  301,
		Time: gtime.NewFromTimeStamp(1720440320000),
		Bool: false,
	})

	// 验证结果
	assert.NoError(t, err)
	user, err := helper.Get("testKey")
	assert.NoError(t, err)
	assert.NotNil(t, user)
	assert.Equal(t, "Alice1", user.Name)
	assert.Equal(t, 301, user.Age)
	assert.Equal(t, int64(1720440320000), user.Time.TimestampMilli())
	assert.Equal(t, false, user.Bool)

	err = g.Redis().HMSet(ctx, "testKey", g.Map{
		"name": "Alice",
		"age":  30,
		"time": 1688774400000,
		"bool": 1,
	})
	assert.NoError(t, err)
}
