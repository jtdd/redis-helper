package script

import _ "embed"

const (
	ScriptHashMapIUDName = "hash_map_iud"
)

var (
	// ScriptHashMapIUD
	// Hash Map IUD
	//go:embed lua/hash_map_iud.lua
	ScriptHashMapIUD string
)
