local function batch_hmset(key, data)
    return redis.call('HMSET', key, unpack(data))
end

local function arrayToTable(arr)
    local tbl = {}
    for i = 1, #arr, 2 do
        tbl[arr[i]] = arr[i + 1]
    end
    return tbl
end

local function updateHash(key, args)
    local fv = arrayToTable(args)

    -- 删除不再使用的字段
    local cursor = '0'
    local count = 100 -- 每次扫描返回的数量
    local matchPattern = '*'

    repeat
        local scan_result = redis.call('HSCAN', key, cursor, 'MATCH', matchPattern, 'COUNT', count)
        cursor = scan_result[1]
        local fields = scan_result[2]

        if fields and #fields > 0 then
            for _, field in ipairs(fields) do
                --检查字段是否存在于 m 中
                if not fv[field] then
                    redis.call('HDEL', key, field)
                end
            end
        end
    until cursor == '0'

    -- 更新或添加字段
    batch_hmset(key, args)

    -- 返回元素数量
    return #fv
end

return updateHash(KEYS[1], ARGV)