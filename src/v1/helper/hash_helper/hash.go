package hash_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
	"github.com/gogf/gf/v2/util/gutil"
)

// HashHelper Redis Hash类型助手类
type HashHelper struct {
	*base.Base
}

// NewHashHelper 实例化
func NewHashHelper(b *base.Base) *HashHelper {
	return &HashHelper{
		Base: b,
	}
}

// HExists 查看哈希表 key 中，指定的字段是否存在。
func (s HashHelper) HExists(key, field string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HEXISTS", s.GetKey(key), field)

	return
}

func (s HashHelper) HExistsNoError(key, field string) (v *gvar.Var) {
	v, _ = s.HExists(key, field)

	return
}

func (s HashHelper) HExistsBool(key, field string) (b bool, err error) {
	var v *gvar.Var
	v, err = s.HExists(key, field)
	if err != nil {
		return
	}

	b = v.Bool()

	return
}

// HKeys 获取所有哈希表中的字段
func (s HashHelper) HKeys(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HKEYS", s.GetKey(key))

	return
}

// HGet 获取存储在哈希表中指定字段的值。
func (s HashHelper) HGet(key, field string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HGET", s.GetKey(key), field)

	return
}

// HMGet 获取所有给定字段的值
func (s HashHelper) HMGet(key string, fields []string) (v *gvar.Var, err error) {
	if fields == nil || len(fields) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "HMGET", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(fields)...)...)

	return
}

// HGetAll 获取在哈希表中指定 key 的所有字段和值
func (s HashHelper) HGetAll(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HGETALL", s.GetKey(key))

	return
}

// HVals 获取哈希表中所有值
func (s HashHelper) HVals(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HVALS", s.GetKey(key))

	return
}

// HSet 将哈希表 key 中的字段 field 的值设为 value 。
func (s HashHelper) HSet(key, field string, value interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HSET", s.GetKey(key), field, value)

	return
}

// HMSet 同时将多个 field-value (域-值)对设置到哈希表 key 中。
func (s HashHelper) HMSet(key string, fv g.Map) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HMSET", append(g.Slice{s.GetKey(key)}, gutil.MapToSlice(fv)...)...)

	return
}

// HSetNx 只有在字段 field 不存在时，设置哈希表字段的值。
func (s HashHelper) HSetNx(key, field string, value interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HSETNX", s.GetKey(key), field, value)

	return
}

// HDel 删除一个或多个哈希表字段
func (s HashHelper) HDel(key, field string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HDEL", s.GetKey(key), field)

	return
}

func (s HashHelper) HDelOnlyError(key, field string) (err error) {
	_, err = s.HDel(s.GetKey(key), field)

	return
}

func (s HashHelper) HDelM(key string, fields []string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HDEL", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(fields)...)...)

	return
}

// HIncrby 为哈希表 key 中的指定字段的整数值加上增量 increment 。
func (s HashHelper) HIncrby(key string, field string, increment int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HINCRBY", s.GetKey(key), field, increment)

	return
}

// HIncrbyFloat 为哈希表 key 中的指定字段的浮点数值加上增量 increment 。
func (s HashHelper) HIncrbyFloat(key string, field string, increment float64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HINCRBYFLOAT", s.GetKey(key), field, increment)

	return
}

// HLen 获取哈希表中字段的数量
func (s HashHelper) HLen(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "HLEN", s.GetKey(key))

	return
}

// HScan 迭代哈希表中的键值对。批量获取field 【注意】field里需要有通配符 比如：a:*
func (s HashHelper) HScan(key string, field string, callback func(key string, fieldVals g.MapStrStr) error, count ...int) (err error) {
	_count := 100
	if count != nil && len(count) > 0 {
		_count = count[0]
	}

	var v *gvar.Var

	cursor := 0
	for {
		v, err = g.Redis().Do(s.Ctx, "HSCAN", s.GetKey(key), cursor, "MATCH", field, "COUNT", gconv.String(_count))
		if err != nil {
			return
		}
		data := gconv.SliceAny(v)
		var dataSlice []string
		err = gconv.Scan(data[1], &dataSlice)
		if err != nil {
			return
		}

		var dataMap g.MapStrStr
		err = gconv.Scan(dataSlice, &dataMap)
		if err != nil {
			return
		}

		err = callback(s.GetKey(key), dataMap)
		if err != nil {
			return
		}

		cursor = gconv.Int(data[0])
		if cursor == 0 {
			break
		}
	}

	return
}
