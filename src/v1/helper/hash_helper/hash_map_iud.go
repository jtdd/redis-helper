package hash_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"gitee.com/jtdd/redis-helper/src/v1/helper/hash_helper/script"
	"gitee.com/jtdd/redis-helper/src/v1/helper/script_helper"
	"github.com/gogf/gf/v2/frame/g"
)

type HashMapIUDHelper struct {
	*base.Base
	hashHelper         *HashHelper
	scriptRunnerHelper *script_helper.ScriptRunner
}

func NewHashMapIUDHelper(b *base.Base) *HashMapIUDHelper {
	return &HashMapIUDHelper{
		Base:               b,
		hashHelper:         NewHashHelper(b),
		scriptRunnerHelper: script_helper.NewScriptRunner(b),
	}
}

// IUD 插入更新删除
func (s *HashMapIUDHelper) IUD(key string, fv g.Map, matchPattern ...string) (err error) {
	_matchPattern := "*"
	if len(matchPattern) > 0 {
		_matchPattern = matchPattern[0]
	}

	err = s.hashHelper.HScan(key, _matchPattern, func(key string, fieldVals g.MapStrStr) (err error) {
		for field, _ := range fieldVals {
			if _, ok := fv[field]; !ok {
				_, err = s.hashHelper.HDel(key, field)
				if err != nil {
					return
				}
			}
		}

		return
	})
	if err != nil {
		return
	}

	_, err = s.hashHelper.HMSet(key, fv)
	if err != nil {
		return
	}

	return
}

// IUDScript 插入更新删除
func (s *HashMapIUDHelper) IUDScript(key string, fv g.Map) (err error) {
	// 调用script_helper 当心交叉调用 之后再优化
	args := make([]interface{}, 0)
	for field, value := range fv {
		args = append(args, field)
		args = append(args, value)
	}

	_, err = s.scriptRunnerHelper.RunAutoLoadScript(
		script.ScriptHashMapIUDName,
		script.ScriptHashMapIUD,
		[]string{key},
		args)
	if err != nil {
		return
	}

	return
}
