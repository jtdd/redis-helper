package hash_helper

import (
	"errors"
	"reflect"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gctx"
	"github.com/gogf/gf/v2/os/gstructs"
)

type HashStructHelper[T any] struct {
	*base.Base
	hashHelper        *HashHelper
	transactionHelper *base.TransactionHelper
}

const (
	DefaultTag          = "redis"
	DefaultTagHashKey   = "redis_hash_key"
	DefaultTagHashField = "redis_hash_field"
)

var (
	ErrorNoTagFound                = errors.New("no tag found")
	ErrorCacheDataIsNil            = errors.New("CacheData is nil")
	ErrorCacheDataIsEmpty          = errors.New("CacheData is empty")
	ErrorCacheDataIsError          = errors.New("CacheData is error")
	ErrorCacheDataExistedEmptyData = errors.New("CacheData existed empty data")
)

func NewHashStructHelper[T any](b *base.Base) *HashStructHelper[T] {
	if b == nil {
		b = base.NewBase(gctx.New(), "", false)
	}

	return &HashStructHelper[T]{
		Base:              b,
		hashHelper:        NewHashHelper(b),
		transactionHelper: base.NewTransactionHelper(b),
	}
}

// Get 获取数据到结构体
// For example:
//
//	type User struct {
//	    Name string `redis_hash_field:"name"`
//	    Age  int    `redis_hash_field:"age"`
//	}
func (s *HashStructHelper[T]) Get(key string) (data *T, err error) {
	data = new(T)

	var tags []gstructs.Field
	tags, err = gstructs.TagFields(data, []string{DefaultTagHashField})
	if err != nil {
		return
	}

	if tags == nil || len(tags) == 0 {
		err = ErrorNoTagFound
		return
	}

	// redis
	// _, err = s.transactionHelper.Multi()
	// if err != nil {
	// 	return
	// }

	var _tags []gstructs.Field
	var _fields []string
	for _, tag := range tags {
		// if tag.IsNil() {
		// 	continue
		// }

		_tags = append(_tags, tag)

		// var tagName = tag.Name()
		var redisHashField = tag.TagValue
		// var tagType = tag.Type()

		// _, err = s.hashHelper.HGet(key, redisHashField)
		// if err != nil {
		// 	break
		// }
		_fields = append(_fields, redisHashField)
	}

	// if err != nil {
	// 	_, err = s.transactionHelper.Discard()
	// 	return
	// }

	var v *gvar.Var
	// v, err = s.transactionHelper.Exec()
	v, err = s.hashHelper.HMGet(key, _fields)
	if err != nil {
		return
	}

	if v.IsNil() {
		err = ErrorCacheDataIsNil
		return
	}

	var _vArray []*g.Var
	_vArray = v.Vars()

	if _vArray == nil || len(_vArray) == 0 {
		err = ErrorCacheDataIsEmpty
		return
	}

	if len(_vArray) != len(_tags) {
		err = ErrorCacheDataIsError
		return
	}

	genericsVarStructHelper := base.NewGenericsVarStructHelper()

	for i, _v := range _vArray {
		if _v.IsNil() || _v.String() == "" {
			err = ErrorCacheDataExistedEmptyData
			return
		}

		_tag := _tags[i]

		var tagName = _tag.Name() // ex: user.Name
		// var redisHashField = _tag.TagValue // ex: name
		var tagType = _tag.Type().String() // ex: string

		val := reflect.ValueOf(data).Elem()
		err = genericsVarStructHelper.Var2ValueByType(_v, val, tagName, tagType)
		if err != nil {
			return
		}
	}

	return
}

func (s *HashStructHelper[T]) Set(key string, data *T) (err error) {
	var tags []gstructs.Field
	tags, err = gstructs.TagFields(data, []string{DefaultTagHashField})
	if err != nil {
		return
	}

	if tags == nil || len(tags) == 0 {
		err = ErrorNoTagFound
		return
	}

	genericsVarStructHelper := base.NewGenericsVarStructHelper()

	mapFieldsAndValues := make(g.Map)
	for _, tag := range tags {
		if tag.IsNil() {
			continue
		}

		var tagName = tag.Name()          // ex: user.Name
		var redisHashField = tag.TagValue // ex: name
		var tagType = tag.Type().String() // ex: string

		// 获取字段值并转换为interface{}类型，以便于Redis操作
		val := reflect.ValueOf(data).Elem()

		var value interface{}
		value, err = genericsVarStructHelper.Reflect2ValueByType(val, tagName, tagType)
		if err != nil {
			return
		}

		mapFieldsAndValues[redisHashField] = value
	}

	// 使用HMSet方法设置多个字段到Redis的哈希中
	_, err = s.hashHelper.HMSet(key, mapFieldsAndValues)
	if err != nil {
		return
	}

	return
}
