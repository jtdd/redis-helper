package hash_helper

import (
	"context"
	"testing"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/contrib/nosql/redis/v2"
	"github.com/gogf/gf/v2/database/gredis"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/stretchr/testify/assert"
)

func init() {
	// 准备测试环境
	config := gredis.Config{
		Address: "127.0.0.1:6379",
		Db:      15,
		Pass:    "1234",
	}
	gredis.RegisterAdapterFunc(func(config *gredis.Config) gredis.Adapter {
		return redis.New(config)
	})
	gredis.SetConfig(&config)
}

// TestHashMapIUDHelper tests the IUD method of HashMapIUDHelper.
func TestHashMapIUDHelper_IUD(t *testing.T) {
	// Create a test context
	ctx := context.Background()
	b := base.NewBase(ctx, "testPrefix:", true)

	// Create a new HashMapIUDHelper instance with the mock dependencies
	helper := NewHashMapIUDHelper(b)

	// Prepare test data
	fv := g.Map{
		"field2": "value2",
		"field3": "value3",
		"field4": "value4",
		"field5": "value5",
		"field6": "value6",
	}

	// Call the IUD method
	err := helper.IUD("testKey", fv)

	// Assert that there was no error
	assert.NoError(t, err)
}

func TestHashMapIUDHelper_IUDScript(t *testing.T) {
	// Create a test context
	ctx := context.Background()
	b := base.NewBase(ctx, "testPrefix:", true)

	// Create a new HashMapIUDHelper instance with the mock dependencies
	helper := NewHashMapIUDHelper(b)

	// Prepare test data
	fv := g.Map{
		"field1": "value1",
		"field2": "value2",
		"field3": "value3",
		"field4": "value4",
		"field5": "value5",
		"field6": "value6",
	}

	// Call the IUD method
	err := helper.IUDScript("testKey", fv)

	// Assert that there was no error
	assert.NoError(t, err)
}
