package list_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

// ListHelper Redis List类型助手类
type ListHelper struct {
	*base.Base
}

// NewListHelper 实例化
func NewListHelper(b *base.Base) *ListHelper {
	return &ListHelper{
		Base: b,
	}
}

// LIndex 通过索引获取列表中的元素
func (s ListHelper) LIndex(key string, index int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LINDEX", s.GetKey(key), gconv.String(index))

	return
}

// LLen 获取列表长度
func (s ListHelper) LLen(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LLEN", s.GetKey(key))

	return
}

// LPop 移出并获取列表的第一个元素
func (s ListHelper) LPop(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LPOP", s.GetKey(key))

	return
}

// LPush 将一个或多个值插入到列表头部
func (s ListHelper) LPush(key string, values []string) (v *gvar.Var, err error) {
	if values == nil || len(values) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "LPUSH", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}

// LPushX 将一个值插入到已存在的列表头部
func (s ListHelper) LPushX(key string, values []string) (v *gvar.Var, err error) {
	if values == nil || len(values) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "LPUSHX", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}

// LRange 获取列表指定范围内的元素
func (s ListHelper) LRange(key string, start, end int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LRANGE", s.GetKey(key), gconv.String(start), gconv.String(end))

	return
}

// LRem 移除列表元素
func (s ListHelper) LRem(key string, count int64, value string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LREM", s.GetKey(key), gconv.String(count), value)

	return
}

// LSet 通过索引设置列表元素的值
func (s ListHelper) LSet(key string, index int64, value string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LSET", s.GetKey(key), gconv.String(index), value)

	return
}

// LTrim 对一个列表进行修剪(trim)，就是说，让列表只保留指定区间内的元素，不在指定区间之内的元素都将被删除。
func (s ListHelper) LTrim(key string, start, stop int64) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "LTRIM", s.GetKey(key), gconv.String(start), gconv.String(stop))

	return
}

// RPop 移除列表的最后一个元素，返回值为移除的元素。
func (s ListHelper) RPop(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "RPOP", s.GetKey(key))

	return
}

// RPush 在列表中添加一个或多个值到列表尾部
func (s ListHelper) RPush(key string, values []string) (v *gvar.Var, err error) {
	if values == nil || len(values) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "RPUSH", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}

// RPopLPush 移除列表的最后一个元素，并将该元素添加到另一个列表并返回
func (s ListHelper) RPopLPush(keySource, keyDestination string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "RPOPLPUSH", s.GetKey(keySource), s.GetKey(keyDestination))

	return
}

// RPushX 为已存在的列表添加值
func (s ListHelper) RPushX(key string, values []string) (v *gvar.Var, err error) {
	if values == nil || len(values) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "RPUSHX", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}
