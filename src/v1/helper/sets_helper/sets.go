package sets_helper

import (
	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/util/gconv"
)

// SetsHelper Redis Sets类型助手类
type SetsHelper struct {
	*base.Base
}

// NewSetsHelper 实例化
func NewSetsHelper(b *base.Base) *SetsHelper {
	return &SetsHelper{
		Base: b,
	}
}

// SAdd 向集合添加一个成员
func (s SetsHelper) SAdd(key, value string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SADD", s.GetKey(key), value)

	return
}

// SAdds 向集合添加一个或多个成员
func (s SetsHelper) SAdds(key string, values []string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SADD", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}

// SCard 获取集合的成员数
func (s SetsHelper) SCard(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SCARD", s.GetKey(key))

	return
}

// SDiff 返回第一个集合与其他集合之间的差异。
func (s SetsHelper) SDiff(key string, otherKeys []string) (v *gvar.Var, err error) {
	if otherKeys == nil || len(otherKeys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SDIFF", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(s.GetKeys(otherKeys))...)...)

	return
}

// SDiffStore 返回给定所有集合的差集并存储在 destination 中
func (s SetsHelper) SDiffStore(destKey string, keys []string) (v *gvar.Var, err error) {
	if keys == nil || len(keys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SDIFFSTORE", append(g.Slice{s.GetKey(destKey)}, gconv.SliceAny(s.GetKeys(keys))...)...)

	return
}

// SInter 返回给定所有集合的交集
func (s SetsHelper) SInter(key string, otherKeys []string) (v *gvar.Var, err error) {
	if otherKeys == nil || len(otherKeys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SINTER", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(s.GetKeys(otherKeys))...)...)

	return
}

// SInterStore 返回给定所有集合的交集并存储在 destination 中
func (s SetsHelper) SInterStore(destKey string, keys []string) (v *gvar.Var, err error) {
	if keys == nil || len(keys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SINTERSTORE", append(g.Slice{s.GetKey(destKey)}, gconv.SliceAny(s.GetKeys(keys))...)...)

	return
}

// SIsMember 判断 member 元素是否是集合 key 的成员
func (s SetsHelper) SIsMember(key, value string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SISMEMBER", s.GetKey(key), value)

	return
}

// SMembers Redis Smembers 命令返回集合中的所有的成员。 不存在的集合 key 被视为空集合。
func (s SetsHelper) SMembers(key string) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SMEMBERS", s.GetKey(key))

	return
}

// SMove 将 member 元素从 source 集合移动到 destination 集合
func (s SetsHelper) SMove(keySource, keyDestination string, value interface{}) (v *gvar.Var, err error) {
	v, err = g.Redis().Do(s.Ctx, "SMOVE", s.GetKey(keySource), s.GetKey(keyDestination), value)

	return
}

// SPop 移除并返回集合中的一个随机元素
func (s SetsHelper) SPop(key string, count ...int) (v *gvar.Var, err error) {
	if count != nil && len(count) > 0 {
		v, err = g.Redis().Do(s.Ctx, "SPOP", s.GetKey(key), count[0])
	} else {
		v, err = g.Redis().Do(s.Ctx, "SPOP", s.GetKey(key))
	}

	return
}

// SRandmember 返回集合中一个或多个随机数
func (s SetsHelper) SRandmember(key string, count ...int) (v *gvar.Var, err error) {
	if count != nil && len(count) > 0 {
		v, err = g.Redis().Do(s.Ctx, "SRANDMEMBER", s.GetKey(key), count[0])
	} else {
		v, err = g.Redis().Do(s.Ctx, "SRANDMEMBER", s.GetKey(key))
	}

	return
}

// SRem 移除集合中一个或多个成员
func (s SetsHelper) SRem(key string, values []string) (v *gvar.Var, err error) {
	if values == nil || len(values) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SREM", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(values)...)...)

	return
}

// SUnion 返回所有给定集合的并集
func (s SetsHelper) SUnion(key string, otherKeys []string) (v *gvar.Var, err error) {
	if otherKeys == nil || len(otherKeys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SUNION", append(g.Slice{s.GetKey(key)}, gconv.SliceAny(otherKeys)...)...)

	return
}

// SUnionStore 所有给定集合的并集存储在 destination 集合中
func (s SetsHelper) SUnionStore(destKey string, keys []string) (v *gvar.Var, err error) {
	if keys == nil || len(keys) == 0 {
		return
	}

	v, err = g.Redis().Do(s.Ctx, "SUNIONSTORE", append(g.Slice{s.GetKey(destKey)}, gconv.SliceAny(s.GetKeys(keys))...)...)

	return
}

// SScan 批量获取value 【注意】value里需要有通配符 比如：a:*
func (s SetsHelper) SScan(key string, value string, callback func(key string, values []string) error, count ...int) (err error) {
	_count := 100
	if count != nil && len(count) > 0 {
		_count = count[0]
	}

	var v *gvar.Var

	cursor := 0
	for {
		v, err = g.Redis().Do(s.Ctx, "SSCAN", s.GetKey(key), cursor, "MATCH", value, "COUNT", gconv.String(_count))
		if err != nil {
			return
		}
		data := gconv.SliceAny(v)
		var dataSlice []string
		err = gconv.Scan(data[1], &dataSlice)
		if err != nil {
			return
		}

		err = callback(s.GetKey(key), dataSlice)
		if err != nil {
			return
		}

		cursor = gconv.Int(data[0])
		if cursor == 0 {
			break
		}
	}

	return
}
