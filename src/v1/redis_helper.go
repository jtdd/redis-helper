package v1

import (
	"context"

	"gitee.com/jtdd/redis-helper/src/v1/helper/base"
	"gitee.com/jtdd/redis-helper/src/v1/helper/hash_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/key_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/list_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/script_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/sets_helper"
	"gitee.com/jtdd/redis-helper/src/v1/helper/string_helper"
	"github.com/gogf/gf/v2/container/gvar"
	"github.com/gogf/gf/v2/frame/g"
)

// RedisHelper 助手类
type RedisHelper struct {
	*base.Base

	key          *key_helper.KeyHelper
	string       *string_helper.StringHelper
	sets         *sets_helper.SetsHelper
	list         *list_helper.ListHelper
	hash         *hash_helper.HashHelper
	hashMapIUD   *hash_helper.HashMapIUDHelper
	stringScript *string_helper.StringScriptHelper
	transaction  *base.TransactionHelper
	script       *script_helper.ScriptHelper
	scriptRunner *script_helper.ScriptRunner
}

func NewRedisHelper(ctx context.Context, prefix string, isEnabledPrefix bool) *RedisHelper {
	return &RedisHelper{
		Base: base.NewBase(ctx, prefix, isEnabledPrefix),
	}
}

type WatchCallback func(key string, helper *RedisHelper) error

// SetPrefix 设置前缀
func (s *RedisHelper) SetPrefix(prefix string) *RedisHelper {
	s.Base.Prefix = prefix
	return s
}

// SetEnabledPrefix 设置是否启用前缀
func (s *RedisHelper) SetEnabledPrefix(isEnabledPrefix bool) *RedisHelper {
	s.Base.IsEnabledPrefix = isEnabledPrefix
	return s
}

// Watch 监听闭包
func (s *RedisHelper) Watch(key string, callback WatchCallback) (err error) {
	_, err = s.Transaction().Watch(key)
	if err != nil {
		return
	}

	defer func(transaction *base.TransactionHelper) {
		_, err := transaction.UnWatch()
		if err != nil {
			g.Log().Error(s.Ctx, err.Error())
		}
	}(s.Transaction())

	err = callback(key, s)
	if err != nil {
		return
	}

	return
}

type MultiCallback func(helper *RedisHelper) error

// Multi 开启事务
func (s *RedisHelper) Multi(callback MultiCallback) (v *gvar.Var, err error) {
	_, err = s.Transaction().Multi()
	if err != nil {
		return
	}

	err = callback(s)
	if err != nil {
		_, err = s.Transaction().Discard()
		return
	}

	v, err = s.Transaction().Exec()
	if err != nil {
		return
	}

	return
}

func (s *RedisHelper) Key() *key_helper.KeyHelper {
	if s.key == nil {
		s.key = key_helper.NewKeyHelper(s.Base)
	}
	return s.key
}

func (s *RedisHelper) String() *string_helper.StringHelper {
	if s.string == nil {
		s.string = string_helper.NewStringHelper(s.Base)
	}
	return s.string
}

func (s *RedisHelper) Sets() *sets_helper.SetsHelper {
	if s.sets == nil {
		s.sets = sets_helper.NewSetsHelper(s.Base)
	}
	return s.sets
}

func (s *RedisHelper) List() *list_helper.ListHelper {
	if s.list == nil {
		s.list = list_helper.NewListHelper(s.Base)
	}
	return s.list
}

func (s *RedisHelper) Hash() *hash_helper.HashHelper {
	if s.hash == nil {
		s.hash = hash_helper.NewHashHelper(s.Base)
	}
	return s.hash
}

func (s *RedisHelper) HashMapIUD() *hash_helper.HashMapIUDHelper {
	if s.hashMapIUD == nil {
		s.hashMapIUD = hash_helper.NewHashMapIUDHelper(s.Base)
	}
	return s.hashMapIUD
}

func (s *RedisHelper) StringScript() *string_helper.StringScriptHelper {
	if s.stringScript == nil {
		s.stringScript = string_helper.NewStringScriptHelper(s.Base)
	}
	return s.stringScript
}

func (s *RedisHelper) Transaction() *base.TransactionHelper {
	if s.transaction == nil {
		s.transaction = base.NewTransactionHelper(s.Base)
	}
	return s.transaction
}

func (s *RedisHelper) Script() *script_helper.ScriptHelper {
	if s.script == nil {
		s.script = script_helper.NewScriptHelper(s.Base)
	}
	return s.script
}

func (s *RedisHelper) ScriptRunner() *script_helper.ScriptRunner {
	if s.scriptRunner == nil {
		s.scriptRunner = script_helper.NewScriptRunner(s.Base)
	}
	return s.scriptRunner
}

// var (
// 	ctx    = gctx.New()
// 	Helper = NewRedisHelper(ctx)
// 	Redis  = Helper
// )
