````go
package main

import (
	"github.com/gogf/gf/v2/frame/g"
	"github.com/gogf/gf/v2/os/gstructs"
)

type User struct {
	Id   int
	Name string `params:"name" a:"1111"`
	Pass string `my-tag1:"pass1" my-tag2:"pass2" params:"pass" a:"3333"`
}

var user User

func main() {
	m, _ := gstructs.TagFields(user, []string{"a", "params"})
	g.Dump(m)
}
````

````
[
    {
        Value: {},
        Field: {
            Name: "Name",
            PkgPath: "",
            Type: "string",
            Tag: "params:\"name\" a:\"1111\"",
            Offset: 8,
            Index: [
                1,
            ],
            Anonymous: false,
        },
        TagName: "a",
        TagValue: "1111",
    }, {
        Value: {},
        Field: {
            Name: "Pass",
            PkgPath: "",
            Type: < cycle dump 0x13d56c0 > ,
            Tag: "my-tag1:\"pass1\" my-tag2:\"pass2\" params:\"pass\" a:\"3333\"",
            Offset: 24,
            Index: [
                2,
            ],
            Anonymous: false,
        },
        TagName: "a",
        TagValue: "3333",
    }, 
]
````